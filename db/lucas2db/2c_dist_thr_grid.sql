-- compute dist_thr_grid
ALTER TABLE lucas${YEAR} ADD COLUMN dist_thr_grid double precision;
UPDATE lucas${YEAR} as t set dist_thr_grid=sq.dist FROM
(
SELECT p.point_id,
       s.geom_thr,
       s.dist as dist
FROM lucas${YEAR} AS p
JOIN LATERAL (
    SELECT g.point_id,
           g.geom_thr,
           st_distance(g.geom_thr, p.geom_thr) AS dist
    FROM grid AS g
    ORDER BY p.geom_thr <-> g.geom_thr
    LIMIT 1
) AS s
ON true
) AS sq
WHERE t.point_id = sq.point_id;
