#!/usr/bin/env python3

from lib.parser_cli import Parser
from lib.create_views import CreateViews
from lib.merge_points import ColumnTypes

class CreateViewsST(CreateViews, ColumnTypes):
    topics_additional = ['space-time']

    def __init__(self, columns, groups, table, start_year, end_year):
        super().__init__(columns, groups, table)
        ColumnTypes.__init__(self, columns, list(range(start_year, end_year+1, 3)))
        self.timeless += [
            "survey_count",
            "survey_maxdist"
        ]

    def _get_columns(self, name):
        cols = []
        item = self.columns[name]
        if name in self.timeless:
            cols.append(name)
        elif name not in self.additional:
            if name in self.timeless:
                cols.append(name)
            else:
                for year in self.years:
                    cols.append('{}_{}'.format(name, year))

        return cols
        
if __name__ == "__main__":
    parser = Parser(years=True,
        args=[
            { 'dest': 'groups', 'metavar': 'groups', 'type': str,
              'help': 'Groups to be processed'
            },
            { 'dest': 'table', 'metavar': 'table', 'type': str,
              'help': 'Table name'
            }
        ]
    )

    cv = CreateViewsST(parser.columns, parser.groups, parser.table,
                       parser.start_year, parser.end_year)
    cv.build_sql()
