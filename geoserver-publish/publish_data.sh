#!/bin/sh -e

EXTRACTSDIR=$BUILDDIR/html/extracts/

# create extracts
mkdir $EXTRACTSDIR
cp /data/dump.sql.7z $EXTRACTSDIR/db_st_lucas_dump.sql.7z
echo "DB dump file copied: $EXTRACTSDIR"

ogr2ogr -f GPKG $EXTRACTSDIR/${TABLE_NAME}.gpkg \
        "PG:dbname=$POSTGRES_DB host=db user=$MAPSERVR_USER password=$MAPSERVR_PASSWD" \
        ${POSTGRES_SCHEMA}.${TABLE_NAME}
ogr2ogr -f GPKG $EXTRACTSDIR${TABLE_NAME_ST}.gpkg \
        "PG:dbname=$POSTGRES_DB host=db user=$MAPSERVR_USER password=$MAPSERVR_PASSWD" \
        ${POSTGRES_SCHEMA}.${TABLE_NAME_ST}
echo "GPKG extract created in $EXTRACTSDIR"

exit 0
